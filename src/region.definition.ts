export interface SypexRegion {
    country_seek: number;
    id: number;
    name_ru: string;
    name_en: string;
    iso: string;
}
