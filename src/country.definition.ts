export interface SypexCountry {
    id: number;
    iso: string;
    name_ru: string;
    name_en: string;
}
