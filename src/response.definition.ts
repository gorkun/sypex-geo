import {SypexCity} from './city.definition';
import {SypexRegion} from './region.definition';
import {SypexCountry} from './country.definition';

export enum SypexResponseType {
    CITY,
    REGION,
    COUNTRY,
}

export interface SypexResponse {
    city: SypexCity,
    region: SypexRegion,
    country: SypexCountry
}
