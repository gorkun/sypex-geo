# @gorkun/sypex-geo

Unofficial Sypex Geo Client for NodeJS.

Supported ES modules syntax from 0.1.0

## Install

```bash
npm i @gorkun/sypex-geo
```

## Usage

```js
import {SypexGeoClient} from "@gorkun/sypex-geo";

const client = new SypexGeoClient('./SxGeoCity.dat');
const city = client.get('178.162.122.146');
```
