import {defineConfig} from 'vite';
import dts from "vite-plugin-dts";
import {resolve} from 'node:path';

export default defineConfig({
    plugins: [dts()],
    build: {
        target: 'esnext',
        polyfillModulePreload: false,
        lib: {
            entry: resolve(__dirname, './src/index.ts'),
            formats: ['es', 'cjs'],
            name: 'lib',
            fileName: 'build',
        },
        rollupOptions: {
            external: [
                'fs'
            ]
        }
    }
});
