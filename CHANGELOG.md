# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2022-10-03
### Added
- Добавлена поддержка ES Modules 

## [0.0.8] - 2021-10-20
### Changed
- Обновил зависимости
- Изменил namespace проекта
### Fixed
- Исправил описание

## [0.0.7] - 2021-10-11
### Changed
- Перенес код из github
### Added
- Добавил changelog файл
- Добавил автоматическую публикацию в gitlab registry
### Fixed
- Исправил работу с JS кодом после билда
